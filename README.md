Strategisches Management
Lerneinheit 1

## Klausur

- Komplexität erklären != kompliziert, chaotisch, einfach, schwierig
- Management generell problematisch: Personaleinsatz, Planung, Kontrolle
- Welche Instrumente gehören zum Management
- Zugang zu einem populären Managementmodell zu lernen st. gallen
  - drei ebenen der unternehmensführung (Grundprobleme kennen)
    - legitimität (Uber, nestle, VW, ...) Tun ist nicht mehr selbstverständlich
    - normatives management   

### Wiederholungsfragen zu Kapitel 3
1. Welche Aufgaben stehen im Mittelpunkt der strategischen Analyse?
   - Auskunft darüber zu erteilen, wo das Unternehmen derzeit steht
2. Welche Aussagen lassen sich mit Hilfe der SWOT-Analyse ableiten?
   - Wie ist unsere Wettbewerbsposition und zwar unsere relative Wettbewerbsposition zu beurteilen?
3. Welche Bereiche werden durch die sog PEST Analyse erfasst und welche Ergebnisse liefert sie für strategische Analyse?
   - Der politisch-rechtliche Rahmen (P)
   - Der wirtschaftliche Rahmen (E)
   - Der sozio-kulturelle Rahmen (S)
   - Der technologische Rahmen (T)
4. Welche Änderungen in den Wertvorstellungen von Menschen lassen sich nach Rosenstiel/Cornelli erkennen?
   - Abwendung von der Arbeit als eine Pflicht
   - Unterstreichung des Wertes der Freizeit
   - Ablehnung von Bindung, Unterordnung und Verpflichtung
   - Betonung des eigenen (hedonistischen) Lebensgenusses
   - Erhöhung der Ansprüche in Bezug auf eigene Selbstverwirklichungschancen
   - Bejahung der Gleichheit und Gleichberechtigung zwischen den Geschlechtern
   - Betonung der eigenen Gesundheit
   - Hochschätzung einer ungefährdeten und bewahrten Natur
   - Skepsis gegenüber den Werten der Industrialisierung wie zum Beispiel Gewinn, Wirtschaftswachstum, technischer Fortschritt
5. Nennen sie Beispiele für technologische Rahmenbedingungen?
   - Straßen, Schienennetz, Häfen, Flughäfen etc.,
   - Schulen und Universitäten,
   - Forschungsinstitutionen und Forschungseinrichtungen,
   - Förderprogrammen, Subventionen.
6. Michael Porter stellte bei seiner Branchenanalyse fest, dass für die Attraktivität einer Branche nicht nur die Wettbewerbssituation von Bedeutung ist. Welche weiteren Faktoren gehören zu seinem 5-Force-Model?
   - Lieferantenmacht
   - Markteintrittsbedrohung
   - Käufermacht
   - Bedrohung durch Ersatzprodukte
   - Konkurrenz innerhalb der Branche
7. Welche Faktoren verhindern bzw. erschweren den Markteintritt neuer Wettbewerber?
   - Kapitalintensität:
   - Skaleneffekte:
   - Zugang zu Vertriebskanälen
   - Umstellungskosten bei Produktwechsel
   - Käuferloyalität:
   - Staatliche Regulierung
8.  Zeigen sie auf, welchen Markteintrittsbarrieren zu unterscheiden sind?
9.  Wo liegt der Schwerpunkt der internen Analyse und welche Schritte sind dabei zu unterscheiden?
10. Wo setzt die funktionsbezogene Analyse strategischer Potentiale an?
11. Beschreiben sie die Wertschöpfungskette nach Porter.
12. Wie lassen sich Kernkompetenzen identifizieren?
13. Was unter der sog. VRIO-Methode zu verstehen?
14. Wie sind sog. Stärken-/Schwächen-Profile aufgebaut?





## Lerneinheit 3
### S. 44 Lernkontrollfragen

1. Welche Gründe gibt es für eine zunehmende Abkehr von strukturdeterminierenden zu stärker partizipativen Organisationsformen?
2. Wie unterscheidet sich die Managementholding von einer divisionalen Organisation?
3. Was versteht man unter Stabsarbeit?
4. Welche Vor- und Nachteile sind mit einer organisatorischen Trennung des Enscheidungsprozesses verbunden?
5. Wie unterscheiden sich Routineprogramme von Zweckprogrammen
6. Auf welchen Grundprinzipien beruht die Matrixorganisation?
7. Wie unterscheiden sich „organische“ von „mechanistischen“ Organisationsformen?

### S. 76 Lernkontrollfragen

1. Inwieweit lässt sich die Unternehmenskultur als Ausdruck der informalen Struktur eines Unternehmens begreifen?
2. Inwiefern ist die Kultur eines Unternehmens historisch gewachsen?
3. Wo liegt der Unterschied zwischen der 2. und der 3. Ebene im Schein´schen Kulturmodell?
4. Repräsentieren Unternehmensleitlinien die Unternehmenskultur?
5. Welche Grundidee des Kulturwandels liegt dem Modell von Dyer zugrunde?
6. Worin unterscheiden sich die sozialtechnische und die kulturalistische Sichtweise der Unternehmenskultur?


## Vorlesung

### Unternehemensführung & Strategie: Problemorientierte einführung

- Seite 6
  - Dispositiver Faktor (Unternehmer - Manager - Entrepreneur - Stratege)
    - Unternehmer sind Eigentümer
    - Manager werden verdächtigt anders mit Unternehmen umzugehen, da sie nicht Eigentümer sind
    - Entrepreneur sind Menschen die disruptiv sind (etwas Kaputt machen) (schöpferischer Zerstörer)
    - Strategen sind in der Nähe des Entrepreneurs, es geht aber nicht um neues sondern um Wettbewerbsbeständigkeit
    - Planung
    - Steuerung
    - Kontorolle
- S. 15
  - management als Funktion - funktionaler Ansatz
    - Komplex von Aufgaben, die zur Steuerung des Unternehmens eingesetzt werden müssen (Planung, Organisation, ...)
  - Management als Institution - institutioneller Ansatz
    - Gruppe von Personen, die in einer Organisation mit Anweisungsbefugnissen betraut sind
  - Entscheidungsmerkmale
    - Grundsatzcharakter
    - Irreversibilität
    - hohen monetärer Wert
    - Geltung für das gesamte Unternehmen
    - geringer Strukturierungsgrad
    - Wertebeladenheit
- S. 18ff - Management funktionale Sicht
  - Querschnittsfunktion
    - Einkauf
    - Produktion
    - Verkauf
  - Managementfunktionen
    - Planung
      - sog. "PrimärfunktioN" und logischer Ausgangspunkt
      - Nachdenken darüber, was und wie erreicht werden soll
      - Ergebnis dieser Reflekttion wird in Zielen, Rahmenrichtlinien und Verfahrensweisen zur Programmrealisierung festgelegt
    - Organisation
      - Umsetzungsfunktion zur Planung
      - Schaffung plangerechter Einheiten (also insbes. Stelle)
      - Zuweisung von Kompeenzen und Weisungsbefugnissen
      - Koordination der Aufgaben
      - Schaffung eines Koordinationssystems
    - Personaleinsatz
      - Anforderungsgerechte Besetzung der Stelle mit Personal
      - Sicherstellung und Erhaltung der sog. "Humanressourcen" (alle sachlichen Personalaufgaben, bspw. als Rekrutierung oder auch Freisetzung)
    - Führung
      - Permanente, konkrete Veranlassung der Arbeitsführung und ihre zieladäquate Feinsteuerung
      - Aufgaben jeder Führungskraft (Führung im engeren Sinne)
    - Kontrolle
      - Sog. Zwillingsfunktion der Planung, durch die Zielerreichungsgrade gemessen werden
      - Systematischer Vergleich mit Plandaten
      - Abweichungsanalyse
      - Ausgangspunkt für Neuplanung
- S. 21ff - Management institutionelle Sicht
  - Hierarchische Position / Terminologische Kategorie
  - Top Management braucht weniger technische Kompetenz, dafür mehr konzeptionelle Kompetenz
- S. 28f - 
  - "Die Frage, wie es troz Wettbewerb möglich sein, das eine Unternehmung im Vergleich zu anderen Mitanbietenr ...
  - Internationale Wettbewerbsfähigkeit als Leitmotiv des managementhandelns
- S. 33 - Umweltkonstrukt
  - Globale Umwelt
    - Ökonimisch
    - technologisc
    - ökologisc
    - rechlich-politisch
    - gesellschaftlich
  - Aufgabenumwelt (task environment)
    - Staat, Öffentlichkeitn
    - Absatzmärkte (Konsumenten)
    - Beschaffungsmärkte (Lieferanten)
    - Konkurrenz
- S. 35 Kondratieff Zyklen
- S. ff42 Komplexität
  - Entscheidungsproblem und Komplexität
    - Probleme werden nicht erkannt
    - mangelhafte Zielerkennung
    - tendenz zur beschränkung auf ausschnite
    - tendenz zur einseitiger schwerpunktsetung (monokausales Ursaceh-Wirkungsdenken)
    - tendenz zur übersteuerung ...
  - Dynaxity
  - Zwischenfazit
    - Vergangenheit
      - Kontinuität
        - berechenbare Entwicklung, hohe BEdeutung von Erfahrungen
      - Transparenz
        - durchschaubare udn verständliche Vorgänge, hohe Sicherheit
      - Eigendynamik
        - hohe Selbstbestimmung
    - Gegenwart und Zukunft
      - Diskontinuität
        - unberechenbare Entwicklung, Relativierung der Erfahrungen
      - Komplexität
        - schwer durchschaubare, unverständliche Abläuft, Verunsicherung
      - Fremddynamik
        - zunehmende Fremdbestimmung

### Funktionen der Unternehmensführung im Detail
1. Planung
   - management Facts oder Folklore
     - Henry Minzberg
2. Organisation
   - S. 108
     - Organisation, Alternative: Improvisation (Zufall), Disposition (Chef, wann muss ich ...)
     - Organisation ist ein System dauerhaft angelegter Regeln [...]
   - S. 110
     - Unterorganisation - Optimum - Überorganisation
   - S. 111
     - Aufbauorganisation
       - Teilaufgaben
       - Stellen
       - Abteilungen
     - Ablauforganisation
       - Arbeitsgänge
       - Arbeitsprozesse
3. Personaleinsatz
   - 
4. Führung
   - 
5. Kontrolle
   - 

### Gestaltungsebenen der Unternehmensführung
1. Normatices managemen
   - S. ff154f
     - Stakeholdermanagement - Leitbilder
   - S. 154ff - Unternehemnesverfassung/corporste gouvernance
     - Staatsverfassung
     - Unternehmensverfassung
   - S. 160f Corporate Governance
     - Deutsche corporate governance kodex
2. Strategisches Management
3. Opeartives Management

### Strategisches Management im Detail
1. SWOT
   - S. 250f 
     - PIMS 40 fdaktoren sind besonders wichtig
     - 2 insbesondere: Marktwachstumsrate, Marktanteil
2. Stategische Optionen
   - S. 292 Kano-Modell
3. Strategisch Wahl
   - S. 312 Strategiewahl
     - shareholder-value ansatz nach Rappaport: Strategiewahl ist eine Investition, zukünftige Gewinne müssen zu heute abgezinst werden um den Invest beurteilen zu können
4. Stategie Implementiertung
   -  S. ff322 
      - [...]
      - Unternehmenskultur
    - S. 326 ff balanced scorecard - implementierungsfortschritt bemessen
5. Strategiekotrolle
   - S. 330 ff
     - Arten Strategischer Kontrolle
       - Prämissen-Kontrolle
       - Durchführungs-Kontrolle
       - Strategische Überwachung

### Stragegie & Innovation: ausgewählte Aspekte

## !! Klausur !!

1. Unternehmensführung & Strategie: Problematische Einführung
   - in eigenen Worten erklären können was management aus funktioneller und institutionell bedeutet
   - eigene Worte: betriebeliche wettbewerbsfähigkeit, komplexität
   - kleine Nomenklatur (Unternehmer, etc.)
2. Unternehmensführung im detail
   - hauptsächlich planung
   - beschäftigung mit zukunft, trends, ...
3. Gestaltungsebenen der Unternehmensführung
   - Drei management ebenen nach st. gallen
   - in eigenen worten erkläten (normatves managent, legitimität, licence to operate)
   - unternehmenspolitik, -verfassung, -kultur, nach st. gallen
   - balanceproblem zwischen projekten und tagesgeschäft
4. im detail
   - Kapitel 1 ist extrem wichtig !!
     - SWOT
     - FRIU, markes based view, ...
   - Kapitel 2 ist auch wichtig
     - strageie formulieren
     - nach perspektive (gesamtunternemesebene, geschäftsbereichstrategie (ort, schwerpunkt, regeln))
       - eine geschäftsbereichsstrategie gibt antwort auf die drei fragen nach ort schwerpunkt und wettbewerbsregeln
   - Strategiewahl
     - zwei probleme
       - objektive entscheidung
       - akzeptanz in meinem kontext (widerstande im unternehmen)
   - strategieimplementierung
     - ist perse ein problem
       - ~~top management~~
       - mittleres manageme
     - welche aufgaben gibt es
     - wie messe ich implementierung
       - balanced scorecard
     - strategiekontrolle
       - besodere herausforderungen
       - paradoxien
       - keine fehler, sondern fatale konsequenzen, wenn die nicht erkannt und korrigiert werden
5. Innovation
   - eigentlich eine eigene vorlesung
   - heikler begriff: disruption
     - kann sich meine branche durch digigtalisierung komplett auflösen?
   - zuatz (nicht klausurrelevant): Frugale Innovation?
     - bedeutnster trend für deutsche unternehmen
     - sind sehr in china, indien verortet
     - reduzieren auf den kern
     - intensiv zu beobachten in der medizintechnik
     - globale mittelschicht (12mrd€)

Klasusur ohne Hilfsmittel! :crying:
